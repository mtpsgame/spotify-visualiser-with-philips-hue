import { Injectable } from '@angular/core';
import { Log } from '../interfaces/log.interface';
import { ReplaySubject, Observable } from 'rxjs';
import { LogType } from '../enums/log-type.enum';

@Injectable({
    providedIn: 'root'
})
export class LoggingService {

    private newLogSubject: ReplaySubject<Log>;

    constructor() {
        this.newLogSubject = new ReplaySubject<Log>();
    }

    newLog(message: string, logType: LogType): void {
        const date = new Date();
        this.newLogSubject.next({
            date,
            message,
            logType
        });
        console.log(LogType[logType].toString() + ':\n' + date.toString() + ' ' + message);
    }

    onNewLog(): Observable<Log> {
        return this.newLogSubject.asObservable();
    }
}
