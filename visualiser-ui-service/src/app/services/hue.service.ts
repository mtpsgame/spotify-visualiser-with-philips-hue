import { Injectable } from '@angular/core';
import { UrlService, Api } from './url.service';
import { HttpClientService } from './http-client.service';
import { HueLight, HueGroup } from '../interfaces/hue.interface';
import { CredentialsService } from './credentials.service';
import { Subject, Observable } from 'rxjs';
import { LoggingService } from './logging.service';
import { HttpHeaders } from '@angular/common/http';
import { LogType } from '../enums/log-type.enum';

@Injectable({
    providedIn: 'root'
})
export class HueService {

    private bridgeIp: string;
    private authenticated: boolean;
    private authenticatedSubject: Subject<boolean>;

    constructor(private readonly httpClientService: HttpClientService,
                private readonly urlService: UrlService,
                private readonly credentialsService: CredentialsService,
                private readonly loggingService: LoggingService) {
        this.authenticated = false;
        this.authenticatedSubject = new Subject<boolean>();
    }

    private responseContainsError(response: any): boolean {
        return response === null || response === undefined || 'error' in response;
    }

    searchForBridge(): Promise<string> {
        return this.httpClientService.get('https://discovery.meethue.com').then(response => {
            if (this.responseContainsError(response) || response.length === 0) {
                this.bridgeIp = null;
                this.loggingService.newLog('Unable to find a hue bridge', LogType.ERROR);
            } else {
                this.bridgeIp = response[0].internalipaddress;
                this.loggingService.newLog('Found hue bridge at ' + this.bridgeIp, LogType.INFO);
            }
            return this.bridgeIp;
        });
    }

    private getApiHeaders(hueBridgeHeader: boolean = true, usernameHeader: boolean = true): HttpHeaders {
        let headers = new HttpHeaders();
        if (hueBridgeHeader) {
            headers = headers.append('Hue-Bridge-Ip', this.bridgeIp);
        }
        if (usernameHeader) {
            headers = headers.append('Authorization', this.credentialsService.getHueUsername());
        }
        return headers;
    }

    async authenticate(): Promise<boolean> {
        if (this.bridgeIp == null) {
            this.bridgeIp = await this.searchForBridge();
            if (this.bridgeIp == null) {
                return Promise.resolve(false);
            }
        }
        const url = this.urlService.getAPIUrl(Api.HUE, 'authenticate');
        return new Promise<boolean>(resolve => {
            this.httpClientService.get(url, this.getApiHeaders(true, false)).then((response => {
                if (this.responseContainsError(response)) {
                    this.setIsAuthenticated(false);
                    resolve(false);
                } else {
                    this.credentialsService.setHueUsername(response.username);
                    this.setIsAuthenticated(true);
                    resolve(true);
                }
            }));
        });
    }

    checkIsAuthenticated(): Promise<boolean> {
        if (this.bridgeIp == null || this.credentialsService.getHueUsername() == null) {
            this.setIsAuthenticated(false);
            return Promise.resolve(false);
        }
        const url = this.urlService.getAPIUrl(Api.HUE, 'isauthenticated');
        return new Promise<boolean>(resolve => {
            this.httpClientService.get(url, this.getApiHeaders()).then((response) => {
                if (this.responseContainsError(response)) {
                    this.setIsAuthenticated(false);
                    resolve(false);
                } else {
                    this.setIsAuthenticated(true);
                    resolve(true);
                }
            });
        });
    }

    private setIsAuthenticated(authenticated: boolean): void {
        this.authenticated = authenticated;
        this.authenticatedSubject.next(this.authenticated);
        if (!this.authenticated) {
            this.credentialsService.clearHueCredentials();
        }
    }

    isAuthenticated(): boolean {
        return this.authenticated;
    }

    onIsAuthenticatedChange(): Observable<boolean> {
        return this.authenticatedSubject.asObservable();
    }

    getBridgeIp(): string {
        return this.bridgeIp;
    }

    getLight(id: number): Promise<HueLight> {
        if (!this.authenticated) {
            return Promise.reject('Unauthenticated');
        }
        const url = this.urlService.getAPIUrl(Api.HUE, 'lights/' + id.toString());
        return new Promise<HueLight>((resolve, reject) => {
            this.httpClientService.get(url, this.getApiHeaders()).then((response) => {
                if (this.responseContainsError(response)) {
                    reject(response);
                } else {
                    const light: HueLight = response.light;
                    light.id = id;
                    light.selected = false;
                    resolve(light);
                }
            });
        });
    }

    getAllLightsByRoom(): Promise<HueGroup[]> {
        if (!this.authenticated) {
            return Promise.reject('Unauthenticated');
        }
        const url = this.urlService.getAPIUrl(Api.HUE, 'rooms');
        return this.httpClientService.get(url, this.getApiHeaders()).then(response => {
            if (this.responseContainsError(response)) {
                return Promise.reject(response);
            }
            const lightPromises: Promise<HueLight>[] = [];
            const hueGroups: HueGroup[] = [];
            for (const group of response.rooms) {
                const hueGroup: HueGroup = {
                    name: group.name,
                    lights: []
                };
                for (const lightId of group.lights) {
                    lightPromises.push(this.getLight(lightId).then(light => {
                        hueGroup.lights.push(light);
                        return light;
                    }));
                }
                hueGroups.push(hueGroup);
            }
            return Promise.all(lightPromises).then(_ => {
                return hueGroups;
            });
        });
    }

    updateLight(id: number, hue: number, brightness: number = 254): Promise<boolean> {
        if (!this.authenticated) {
            return Promise.reject('Unauthenticated');
        }
        const url = this.urlService.getAPIUrl(Api.HUE, 'lights/' + id.toString());
        const body = {
            on: true,
            bri: brightness,
            hue,
            transitiontime: 0
        };
        return new Promise<boolean>((resolve, reject) => {
            this.httpClientService.put(url, body, this.getApiHeaders()).then((response) => {
                if (this.responseContainsError(response)) {
                    reject(false);
                } else {
                    resolve(true);
                }
            });
        });
    }
}
