import os
import sys

import requests
from flask import Flask, redirect, request
from flask_cors import CORS
from flask_restful import Api, Resource
from flask_restful.reqparse import RequestParser
from requests.auth import HTTPBasicAuth


def ensure_authorisation_params_are_provided():
    """
    Called before every request to ensure the caller has provided an access token.
    :return: An error if the caller failed to provide these, otherwise None.
    """
    if not request.method == 'OPTIONS':
        endpoint = str(request.url).split('?')[0]
        if not endpoint.endswith(ENDPOINT_AUTHENTICATE) and not endpoint.endswith(ENDPOINT_AUTHENTICATE_CALLBACK):
            if 'Authorization' not in request.headers:
                return {'error': 'Authorization header not found in request'}, 400


def make_request_to_spotify_api(method, endpoint, body=None):
    """
    Makes a HTTP request to the spotify api, given the access token from the request parameters.
    :param method: Type of HTTP request as a string, e.g. GET.
    :param endpoint: The endpoint to make the request to (as a string).
    :param body: The body of the request (assumed to be in a json format).
    :return: The json response and status code from the spotify api.
    """
    response = requests.request(method, 'https://api.spotify.com/v1/{}'.format(endpoint),
                                headers={'Authorization': request.headers['Authorization']}, json=body)

    if response.status_code == 204:
        json_response = {}
    else:
        json_response = response.json()

    return json_response, response.status_code


def unauthorised_error():
    """
    Called when an invalid access token is being used.
    :return: An error to indicate an invalid access token.
    """
    return {'error': 'Invalid access token'}, 401


def is_response_error(response):
    """
    Checks whether the response from the spotify api indicates an error occurred.
    :param response: The json response to check.
    :return: True if response represents an error, False otherwise.
    """
    return 'error' in response


def is_response_unauthorised(status_code):
    """
    Checks whether the status code from a spotify api response indicates the access token in unauthorised
    :param status_code: The status code from the spotify api.
    :return: True if unauthorised, False otherwise.
    """
    return status_code == 401


def print_responses(response):
    """
    If debug mode is on then the JSON API response is printed to the console.
    :param response: The Flask response object.
    :return: The same response object, unmodified.
    """
    if os.environ['DEBUG'] == 'true' and response.content_type == 'application/json':
        print('\n\n' + response.get_data(True))
        sys.stdout.flush()
    return response


class Authenticate(Resource):
    """
    Authentication endpoint.
    """

    def get(self):
        """
        The GET endpoint.
        Constructs the URL that the user should be redirected to, should they want to authenticate spotify.
        :return: The URL to allow user authentication.
        """
        # Retrieve the generated state string.
        parser = RequestParser()
        parser.add_argument('state')
        state = parser.parse_args()['state']
        if not state:
            return {'error': 'Missing the state parameter'}, 400

        # Construct the URL and return it.
        url = 'https://accounts.spotify.com/authorize?client_id=' + os.environ['SPOTIFY_CLIENT_ID'] + \
              '&response_type=code&redirect_uri=http://' + os.environ['HOST_IP'] + \
              ':17001/authenticate/callback&state=' + state + \
              '&scope=user-read-playback-state%20user-modify-playback-state&show_dialog=true'
        return {'status': 'Constructed authentication url successfully', 'url': url}, 200

    def post(self):
        """
        The POST endpoint.
        Generates a new access token, assuming a valid refresh token was provided in the query params.
        :return: The new access token, or an error if the refresh token was invalid.
        """
        # Get the refresh token.
        if 'refresh_token' not in request.json:
            return {'error': 'Missing the refresh_token body parameter'}, 400

        # Tell spotify to generate a new access token.
        response = requests.post('https://accounts.spotify.com/api/token',
                                 data={'grant_type': 'refresh_token', 'refresh_token': request.json['refresh_token']},
                                 auth=HTTPBasicAuth(os.environ['SPOTIFY_CLIENT_ID'],
                                                    os.environ['SPOTIFY_CLIENT_SECRET'])).json()

        # Detect if there were errors and return the new access token.
        if 'error' in response:
            return {'error': response['error_description']}, 400

        return {'status': 'Generated new access token successfully', 'access_token': response['access_token']}, 200


class Play(Resource):
    """
    Resume user playback endpoint.
    """

    def put(self):
        """
        The PUT endpoint.
        Starts / resumes playback of the most recently played track.
        :return: A successful status if the playback started, or an error otherwise.
        """
        # Issue the play command.
        response, status = make_request_to_spotify_api('PUT', 'me/player/play')

        # Return an error if appropriate.
        if is_response_unauthorised(status):
            return unauthorised_error()
        elif is_response_error(response):
            if status == 404:
                return {'error': 'No device was found to play the spotify track'}, status
            elif status == 403 and response['error']['reason'] == 'PREMIUM_REQUIRED':
                return {'error': 'Non-premium spotify accounts cannot be used to play specified tracks'}, status
            elif response['error']['reason'] != 'ALREADY_PLAYING':
                return {'error': 'An unknown error occurred'}, 400

        return {'status': 'Started playback successfully'}, 200


class Pause(Resource):
    """
    Pauses user playback endpoint.
    """

    def put(self):
        """
        The PUT endpoint.
        Pauses playback of the currently playing track.
        :return: A successful status if the playback was paused, or an error otherwise.
        """
        # Issue the play command.
        response, status = make_request_to_spotify_api('PUT', 'me/player/pause')

        # Return an error if appropriate.
        if is_response_unauthorised(status):
            return unauthorised_error()
        elif is_response_error(response):
            if status == 404:
                return {'error': 'No device was found to pause playback'}, status
            elif status == 403 and response['error']['reason'] == 'PREMIUM_REQUIRED':
                return {'error': 'Non-premium spotify accounts cannot be used to pause specified tracks'}, status
            elif response['error']['reason'] != 'ALREADY_PAUSED':
                return {'error': 'An unknown error occurred'}, 400

        return {'status': 'Paused playback successfully'}, 200


class CurrentlyPlaying(Resource):
    """
    Currently playing track endpoint.
    """

    def get(self):
        """
        The GET endpoint.
        Fetches high level information about the currently playing track, or a blank 204 response if no track is
        playing.
        :return: Track information or 204 no content.
        """
        # Get the currently playing track.
        response, status = make_request_to_spotify_api('GET', 'me/player/currently-playing')

        # Return an error if appropriate.
        if is_response_unauthorised(status):
            return unauthorised_error()
        elif is_response_error(response):
            return {'error': 'An unknown error occurred'}, 400

        # If no track is being played then return nothing.
        if status == 204:
            return {}, 204

        return {'status': 'Fetched currently playing track successfully',
                'progress_ms': response['progress_ms'],
                'is_playing': response['is_playing'],
                'track_id': response['item']['id']}, 200


class Track(Resource):
    """
    Track endpoint.
    """

    def get(self, id):
        """
        The GET endpoint.
        Fetches metadata about the specified track.
        :param id: The spotify ID of the track to gather the metadata of.
        :return: Track metadata.
        """
        # Get the audio analysis for the specified track.
        response, status = make_request_to_spotify_api('GET', 'tracks/{}'.format(id))

        # Return an error if appropriate.
        if is_response_unauthorised(status):
            return unauthorised_error()
        if is_response_error(response):
            if status == 400:
                return {'error': 'Invalid track id'}, 400
            elif status == 404:
                return {'error': 'No track with the specified id was found'}, 404
            return {'error': 'An unknown error occurred'}, 400

        return {'status': 'Fetched track successfully',
                'id': response['id'],
                'name': response['name'],
                'artists': ', '.join(artist['name'] for artist in response['artists']),
                'duration_ms': response['duration_ms']}, 200


class AudioAnalysis(Resource):
    """
    Audio analysis endpoint.
    """

    def get(self, id):
        """
        The GET endpoint.
        Fetches low level audio analysis information about the specified track.
        :param id: The spotify ID of the track to gather the audio analysis of.
        :return: Track audio analysis.
        """
        # Get the audio analysis for the specified track.
        response, status = make_request_to_spotify_api('GET', 'audio-analysis/{}'.format(id))

        # Return an error if appropriate.
        if is_response_unauthorised(status):
            return unauthorised_error()
        if is_response_error(response):
            if status == 400:
                return {'error': 'Invalid track id'}, 400
            elif status == 404:
                return {'error': 'No audio analysis available for track'}, 404
            return {'error': 'An unknown error occurred'}, 400

        return {'status': 'Fetched audio analysis successfully',
                'tempo': response['track']['tempo'],
                'beats': [int(beat['start'] * 1000) for beat in response['beats']]}, 200


def setup_authentication_callback(app, route):
    """
    Attaches the authentication callback endpoint to the app.
    :param app: The Flask app to attach the callback to.
    :param route: The endpoint path to reach the callback.
    """

    @app.route(route)
    def authenticate_callback():
        """
        Authentication callback endpoint.
        Once the user has allowed or blocked this application access to their spotify account, they will be redirect to
        this endpoint.
        :return: A redirect back to the visualiser on success, or an error otherwise.
        """
        args = request.args
        if 'error' in args:
            return {'error': args['error']}, 400
        if 'code' not in args:
            return {'error': 'No code was provided'}, 400

        # Get the first access token along with the refresh token and its expiry time.
        response = requests.post('https://accounts.spotify.com/api/token',
                                 data={'grant_type': 'authorization_code', 'code': args['code'],
                                       'redirect_uri':
                                           'http://' + os.environ['HOST_IP'] + ':17001/authenticate/callback',
                                       'client_id': os.environ['SPOTIFY_CLIENT_ID'],
                                       'client_secret': os.environ['SPOTIFY_CLIENT_SECRET']}).json()

        # Redirect the user back to the visualiser ui service, providing the refresh token.
        return redirect(
            'http://{}:17010?spotify_refresh_token={}&state={}'.format(os.environ['HOST_IP'], response['refresh_token'],
                                                                       request.args['state'])), 302


# Start flask.
if __name__ == '__main__':
    # Constants.
    ENDPOINT_AUTHENTICATE = '/authenticate'
    ENDPOINT_AUTHENTICATE_CALLBACK = '/authenticate/callback'
    ENDPOINT_PLAY = '/play'
    ENDPOINT_PAUSE = '/pause'
    ENDPOINT_CURRENTLY_PLAYING = '/currentlyplaying'
    ENDPOINT_TRACK = '/track/<id>'
    ENDPOINT_AUDIO_ANALYSIS = '/audioanalysis/<id>'

    # Flask setup.
    app = Flask(__name__)
    CORS(app)
    api = Api(app)

    # Pre-request checks.
    app.before_request(ensure_authorisation_params_are_provided)

    # After-request utils.
    app.after_request(print_responses)

    # Establish endpoints.
    api.add_resource(Authenticate, ENDPOINT_AUTHENTICATE)
    api.add_resource(Play, ENDPOINT_PLAY)
    api.add_resource(Pause, ENDPOINT_PAUSE)
    api.add_resource(CurrentlyPlaying, ENDPOINT_CURRENTLY_PLAYING)
    api.add_resource(Track, ENDPOINT_TRACK)
    api.add_resource(AudioAnalysis, ENDPOINT_AUDIO_ANALYSIS)

    # Establish authentication callback
    setup_authentication_callback(app, ENDPOINT_AUTHENTICATE_CALLBACK)

    # Run flask
    app.run(host='0.0.0.0', port=80, debug=(os.environ['DEBUG'] == 'true'), threaded=False, processes=25)
